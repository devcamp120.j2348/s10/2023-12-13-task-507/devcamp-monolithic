// Khai báo thư viện ExpressJS
// import express from "express"
const express = require("express");

//khai báo thư viện path
const path = require("path");

// Khai báo app express
const app = express();

// Khai báo cổng chạy API
const port = 8000;

//khai báo để sử dụng các tài nguyên tĩnh (image, css, js,...)
app.use(express.static("views"))

// Khai báo API GET /
app.get("/", (request, response) => {
    /*
    let today = new Date();

    response.json({
        message: `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })*/

    //response.send("<div style='font-weight:bold;color:red;font-size:20px'>Hello world!</div>");
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/views/index.html"));
})

app.get("/about", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/views/about.html"));
})

app.get("/sitemap", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/views/sitemap.html"));
})

// Chạy app express trên cổng 8000
app.listen(port, () => {
    console.log(`App đã được chạy trên cổng ${port}`);
})